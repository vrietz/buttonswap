/*
 * ButtonSwap (CPP) - Version 0.9.6
 * Small utility to invert mouse buttons.
 * Copyright (C) 2021 by Vincent Rietz
 *
 * This program is licensed under the MIT License. Check the LICENSE file
 * for more information or visit <https://opensource.org/licenses/MIT>.
 */

#include "stdafx.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow) {
    bool currentlySwapped = GetSystemMetrics(SM_SWAPBUTTON) != 0;
    SwapMouseButton(currentlySwapped ? FALSE : TRUE);

    LPCWSTR message = (currentlySwapped) 
        ? L" Mouse buttons have been inverted!\n Primary button: Left." 
        : L" Mouse buttons have been inverted!\n Primary button: Right.";
    MessageBox(NULL, message, L"ButtonSwap � Vincent Rietz", MB_OK | MB_ICONINFORMATION);

    return 0;
}
