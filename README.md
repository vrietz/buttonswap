# ButtonSwap
Minimal WinAPI program to invert left and right mouse buttons.

## Requirements
As being developed using the Visual C++ WinAPI, this program can only be run on Windows systems.

## Usage
Simply download and execute `Release/ButtonSwap.exe`. Mouse buttons will be inverted immediately.

## Build release
To build the software yourself, use Visual Studio 2015 or newer and include all source, precompiled headers and resource files.

## License
The project is published under the MIT License.
